from cs50 import SQL
from flask import Flask, flash, redirect, render_template, request, session
from flask_session import Session
from tempfile import mkdtemp
from werkzeug.exceptions import default_exceptions
from werkzeug.security import check_password_hash, generate_password_hash
from datetime import datetime
import string
import hashlib


from helpers import apology, login_required, lookup, usd

# Configure application
app = Flask(__name__)

#https://tinyurl.com/y7aaenxn

# Ensure responses aren't cached


@app.after_request
def after_request(response):
    response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    response.headers["Expires"] = 0
    response.headers["Pragma"] = "no-cache"
    return response

# Configure session to use filesystem (instead of signed cookies)
app.config["SESSION_FILE_DIR"] = mkdtemp()
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)

# Configure CS50 Library to use SQLite database
db = SQL("sqlite:///data.db")

@app.route("/")
def landing():
    return render_template("home.html")

@app.route("/login-register", methods=["POST","GET"])
def entry():
    if request.method == "POST":
        first_name = request.form.get("signupFname")
        last_name = request.form.get("signupLname")
        email = request.form.get("signupEmail")
        password = (request.form.get("signupPass"))
        hash_object = hashlib.sha256(password)
        hex_dig = hash_object.hexdigest()

        result = db.execute("INSERT INTO users (firstname,lastname,email,password) VALUES (:firstname,:lastname,:email,:passw)",
                                    firstname=first_name, lastname=last_name, email=email, passw=hex_dig)

        if not result:
            return apology("Username already exists", 400)
        else:
            # Query database for username
            rows = db.execute("SELECT * FROM users WHERE firstname=:firstname AND lastname=:lastname",
                              firstname=first_name, lastname=last_name)
            session["user_id"] = 2
            return redirect("/mainpage")
    else:
        return render_template("login-register.html")


@app.route("/mainpage")
def home():
    return render_template("mainpage.html")

@app.route("/dowork", methods=["POST", "GET"])
def dowork():
    if request.method == "POST":
        service = request.form.get("service")
        db.execute("INSERT INTO services (id, service) VALUES (:id, :service)", id=2, service=service)
        return redirect("/registered")
    else:
        return render_template("dowork.html")

@app.route("/registered", methods=["POST", "GET"])
def registered():
    return render_template("registered.html")

@app.route("/getwork", methods=["POST", "GET"])
def getwork():
    if request.method == "POST":
        service = request.form.get("service")
        db.execute("INSERT INTO services (id, service) VALUES (:id, :service)", id=2, service=service)
        return render_template("registered.html")
    else:
        return render_template("getwork.html")


@app.route("/logout")
def logout():
    """Log user out"""

    # Forget any user_id
    session.clear()

    # Redirect user to login form
    return redirect("/")